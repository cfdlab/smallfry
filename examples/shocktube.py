import numpy as np

# smallFRy imports
from smallfry.grid.grid1d import Grid1D
from smallfry.solver.solver import Solver

from smallfry.solver.rhs_eval import CockburnShuLimiterMUSCL, BSBLimiter
from smallfry.solver.rhs_eval import Euler1DFlux
from smallfry.solver.rhs_eval import NodalDGEval1D, FluxReconstructionEval1D

def init_sodshock(grid, gamma=1.4):
    gamma1 = gamma - 1.0

    q = grid.uh
    
    rhol, rhor = 1.0, 0.125
    pl, pr = 1.0, 0.1
    ul = ur = 0.0

    El, Er = pl/gamma1, pr/gamma1

    for i in range( grid.ncells ):
        xh = grid.xh[ i ]

        xc = np.sum(xh)/len(xh)
        if xc < 0.5:
            q[i, 0, :] = rhol
            q[i, 1, :] = 0.0
            q[i, 2, :] = El
        else:
            q[i, 0, :] = rhor
            q[i, 1, :] = 0.0
            q[i, 2, :] = Er

# create the grid and the initial solution
g = Grid1D(xl=0.0, xr=1.0, ncells=250, order=2, nvar=3)
init_sodshock(g)

# Flux and limiter function
flux = Euler1DFlux(g)
limiter = CockburnShuLimiterMUSCL(g)
#limiter = BSBLimiter(g)

# Evaluator for the RHS (FR/DG/SD etc)
#rhs_eval = NodalDGEval1D(g, flux)
rhs_eval = FluxReconstructionEval1D(g, flux)

# set the limiter
rhs_eval.set_limiter(limiter)

# construct the solver
solver = Solver(dim=1, grid=g, tf=0.15, cfl=0.3)
solver.set_rhs_eval(rhs_eval)

# solve
solver.setup()
solver.solve()
