import numpy as np

# smallFRy imports
from smallfry.grid.grid1d import Grid1D
from smallfry.solver.solver import Solver

from smallfry.solver.rhs_eval import CockburnShuLimiterMUSCL, BSBLimiter
from smallfry.solver.rhs_eval import Advection1DFlux
from smallfry.solver.rhs_eval import FluxReconstructionEval1D, NodalDGEval1D,\
    FVMEval1D

diffusion=True

pi = np.pi
xl = -pi; xr = pi; ncells = 10; order = 3; nvar=1
tf=1.0

if not diffusion:
    xl = -1.0
    xr = +1.0
    tf = 20.0

def u0(x):
    """Initial Gaussian hump Eq. (4.2) in [VCJ11]"""
    return np.exp( -20*x*x )

def advec_diffusion(x):
    """Initial soltuion for the Advection diffusion equation"""
    return np.sin(x)

# create the grid and initial conditions
g = Grid1D(xl=xl, xr=xr, ncells=ncells, order=order, nvar=nvar)

for i in range(g.ncells):
    if diffusion:
        g.uh[i, 0][:] = advec_diffusion( g.xh[i] )
    else:
        g.uh[i, 0][:] = u0(g.xh[i] )

# Evaluator for the RHS (FR/DG/SD etc)
flux = Advection1DFlux(g, a=1.0, kappa=1.0)
limiter = CockburnShuLimiterMUSCL(g)
#limiter = BSBLimiter(g)

rhs_eval = FluxReconstructionEval1D(g, flux, diffusion=diffusion)
#rhs_eval = NodalDGEval1D(g, flux)
#rhs_eval = FVMEval1D(g, flux)

#rhs_eval.set_limiter(limiter)

# construct the solver
solver = Solver(dim=1, grid=g, tf=tf, cfl=0.005, pfreq=1000) 
solver.set_rhs_eval(rhs_eval)

# solve
solver.setup()
solver.solve()
