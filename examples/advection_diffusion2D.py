import numpy as np

# smallFRy imports
import smallfry.grid.grid2d as grid 
from smallfry.solver.solver import Solver

from smallfry.solver.flux import AdvectionDiffusion2DFlux
from smallfry.solver.rhs_eval2d import FluxReconstructionEval2D

diffusion=True

pi = np.pi
xl = yl = -pi; xr = yr = pi; nx = ny = 10; order = 4; nvar=1
tf=1.0

if not diffusion:
    xl = yl = -1.0
    xr = yr = +1.0
    tf = 2.0

def u0(x, y):
    """Initial Gaussian hump Eq. (4.2) in [VCJ11]"""
    return np.exp( -20*(x**2 + y**2) )

def advec_diffusion(x, y):
    """Initial soltuion for the Advection diffusion equation"""
    return np.sin(x)*np.sin(y)

# create the grid and initial conditions
g = grid.TensorGrid2D(xl, xr, yl, yr, nx, ny, order, nvar)

for i in range(g.nx):
    for j in range(g.ny):
        if not diffusion:
            g.uh[i, j, 0][:] = u0(g.xh[i, j], g.yh[i, j])
        else:
            g.uh[i, j, 0][:] = advec_diffusion(g.xh[i, j], g.yh[i, j])

# Evaluator for the RHS (FR/DG/SD etc)
flux = AdvectionDiffusion2DFlux(g, kappa=1.0, beta=0.5)
rhs_eval = FluxReconstructionEval2D(g, flux, diffusion=diffusion)

# construct the solver
solver = Solver(dim=2, grid=g, tf=tf, cfl=0.005, pfreq=100) 
solver.set_rhs_eval(rhs_eval)

# solve
solver.setup()
solver.solve()
