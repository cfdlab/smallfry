r"""Implementation for 1D grids used in DG and FR type schemes"""

# NumPy
import numpy as np

# module for working with polynomials
import polynomials as poly

# Distribution of solution nodes within a reference element
class NodeDistribution:
    Uniform = 0
    Gauss = 1
    GaussLobatto = 2

# Boundary conditions
class BoundaryCondition:
    Periodic = 0
    Transmissive = 1
    Wall = 2

class LeftBC(BoundaryCondition):
    pass

class RightBC(BoundaryCondition):
    pass

class Grid1D(object):
    r"""One-dimensional grid

    Data Attributes:

      - dx : double
          Uniform grid spacing
          
      - ncells, nfaces : int 
          Number of cells and faces

      - k, kp1 : int
          Scheme order and number of solution nodes on the reference element

      - nvar : int
          Number of solution variables.

      - x : array (nfaces,1)
          Node coordinates

      - uh, uh1, uh2, unew : array (ncells, nvar, kp1)
          Solution vectors

      - k1, k2, k3, k4 : array(ncells, nvar, kp1)
          RHS storage vectors

      - fd, fc : array(ncells, nvar, kp1)
          Discontinuous and continuous flux at the solution nodes

      - fi : array(nfaces, nvar)
          Interface/Interaction flux

      - xr : array(kp1, 1)
          Solution nodes on the reference element

      - xh : array(ncells, kp1)
          Physical coordinates for the solution nodes

      - ex : array(ncells, 2)
          Physical coordinates for the element end points

      - Jn : array(ncells, kp1)
          Element transformation Jacobian

      - dr : array(kp1, kp1)
          Lagrane polynomial differentiation matrix

      - e2f : array(ncells, 2)
          Element to face mapping
          
      - f2e : array(nfaces, 2)
          Face to element mapping
    
    The grid is defined by `ncell` cells and `nfaces` faces:

        0   1   2   3 ...         nf-1   nf  
        ---------------------------------
        |   |   |   | ...  |   |   |   |
        |   |   |   | ...  |   |   |   |
        |   |   |   | ...  |   |   |   |
        ---------------------------------
          0   1   2   ...       nc-1  nc 

          
    The first (index 0) and the last (ncells-1) cells are used to
    implement the boundary conditions.

    The solution is stored on the grid in the form of NumPy
    arrays. Each element has `k+1` nodes for a kth order
    scheme. Additionally, we may be solving for systems of
    equations. To represent this, the solution vector is stored as an
    array of shape (ncells X nvar X k+1). Thus, the solution vector
    for all variables for a given cell can be accessed as

    grid.uh[ cell_index ] for 0 <= cell_index < ncells

    """
    def __init__(
        self, xl, xr, ncells, order, nvar=1, 
        nodes=NodeDistribution.GaussLobatto, 
        left_bc=LeftBC.Periodic, right_bc=RightBC.Periodic):

        # grid spacing
        self.dx = dx = float(xr-xl)/ncells
        
        # add two ghost elements
        self.ncells = ncells + 2
        
        # number of nodes/faces
        self.nfaces = nfaces = self.ncells + 1 
        
        # create the grid
        self.x = x = np.linspace(xl-dx, xr+dx, nfaces)

        # solution order
        self.k = k = order
        self.kp1 = kp1 = order + 1

        # type of node distribution
        self.nodes = nodes

        # boundary conditions at either end
        self.left_bc = left_bc
        self.right_bc = right_bc
        
        # Arrays used. Each element has K+1 solution nodes. The total
        # number of solution points is ncells X (K+1) which is stored
        # as a 2D array per solution variable. The solution at the nth
        # element can be accessed as >>> uh[n], for 0 <= n <
        # ncells-1. Along with the solution u^h, we also store the
        # physical points for each cell in `xh` and the discontinuous
        # and continuous fluxes in `fd` and `fi`, `fc` respectively.
        self.nvar = nvar

        # solution at the nodes and rhs storage vectors
        self.uh = np.zeros(shape=(self.ncells, nvar, kp1))
        self.u0 = self.uh.copy()
        self.uh1= self.uh.copy()
        self.uh2= self.uh.copy()
        self.k1 = self.uh.copy()
        self.k2 = self.uh.copy()

        # cell averages and cell centers
        self.uavg = np.zeros( shape=(self.ncells, nvar) )
        self.xc = np.zeros( shape=(self.ncells) )

        # new solution vector
        self.unew = self.uh.copy()

        # auxillary solution variable and common solution value
        self.q =  np.zeros(shape=(self.ncells, nvar, kp1))
        self.uc = np.zeros(shape=(self.nfaces, nvar))

        # modal coefficients
        self.uhat = self.uh.copy()

        # physical coordinates of the nodes
        self.xh = np.zeros(shape=(self.ncells, kp1))

        # discontinuous and continuous flux at the nodes
        self.fd = np.zeros(shape=(self.ncells, nvar, kp1))
        self.fc = np.zeros(shape=(self.ncells, nvar, kp1))

        # interface flux values
        self.fi = np.zeros(shape=(self.nfaces, nvar))

        ####### Grid related arrays #######
        # element physical end points
        self.ex = np.zeros(shape=(self.ncells, 2))

        # face to element mapping
        self.f2e = np.zeros(shape=(self.nfaces, 2), dtype=int)

        # element to face mapping
        self.e2f = np.zeros(shape=(self.ncells, 2), dtype=int)

        # initialize the grid
        self.initialize()

        # Vandermode and Mass matrix
        self.V = V = poly.get_vandermode1d(self.xr, self.k)
        self.Vinv = np.linalg.inv(V)
        self.Minv = V.dot(V.T)

    def initialize(self):
        self._get_node_distribution()
        self._construct_element_mappings()
        self._construct_grid()

    def compute_cell_averages(self, u):
        for i in range(self.ncells):
            
            self.xc[i] = self.xh[i, 0] + 0.5 * self.dx

            for var in range(self.nvar):
                uh = self.Vinv.dot( u[i, var, :] )
                uh[1:] = 0.0

                # cell averages
                self.uavg[i, var] = self.V.dot(uh)[0]

    def get_modal_coefficients(self, u):
        for i in range(self.ncells):
            for var in range(self.nvar):
                self.uhat[i, var, :] = self.Vinv.dot( u[i, var, :] )

    def get_nodal_coefficients(self, uhat, u):
        for i in range(self.ncells):
            for var in range(self.nvar):
                u[i, var, :] = self.V.dot( uhat[i, var, :] )

    def _get_node_distribution(self):
        """Get the node distribution on the reference element"""
        # k+1 nodes on the reference element
        if self.nodes == NodeDistribution.GaussLobatto:
            self.xr = poly.get_gauss_lobatto_nodes(self.k)

        # Lagrange Ploynomial derivative matrix
        self.dr = poly.get_lagrange_derivative_matrix(self.xr, self.k)

    def _construct_element_mappings(self):
        """Construct the face to element and element to face mappings"""
        nfaces = self.nfaces
        ncells = self.ncells
        f2e = self.f2e
        e2f = self.e2f

        # construct the face to element mappings. The ghost nodes are
        # used to impose the boundary conditions on the left and right
        # ends. A negative index means an invalid index
        for i in range(nfaces):
            if i == 0:
                f2e[i, 0] = -1
                f2e[i, 1] = i
            elif i == nfaces-1:
                f2e[i, 0] = i-1
                f2e[i, 1] = -1
            else:
                f2e[i, 0] = i-1
                f2e[i, 1] = i

        # construct the element to face mappings
        for i in range(ncells):
            e2f[i, 0] = i
            e2f[i, 1] = i+1
            
    def _construct_grid(self):
        """Construct the grid"""
        ncells = self.ncells
        ex = self.ex
        xh = self.xh
        x = self.x

        dxmin = self.dx

        # element physical end points
        for i in range(ncells):
            ex[i,0] = x[i]
            ex[i,1] = x[i+1]
        
            # physical locations for the interior nodes
            xh[i][:] = 0.5 * (1-self.xr)*ex[i,0] + 0.5 * (1+self.xr)*ex[i,1]

            # get minimum dx for time step control
            if self.k > 0:
                dxmin = min(dxmin, np.min(xh[i, 1:] - xh[i, :-1]))

        self.dxmin = dxmin

        # Jacobian of the transformation
        self.Jn = Jn = np.ones( shape=(ncells, self.kp1) ) * 0.5 * self.dx

        if self.k > 0:
            for i in range(ncells):
                Jn[i][:] = self.dr.dot( self.xh[i] )
