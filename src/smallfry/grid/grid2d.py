r"""Implementation for 1D grids used in DG and FR type schemes"""

# NumPy
import numpy as np

# module for working with polynomials
import polynomials as poly

class TensorGrid2D(object):
    r"""One-dimensional grid

    Data Attributes:

      - dx, dy : double
          Uniform grid spacing
          
      - nx, ny : int 
          Number of cells and faces

      - k, kp1, Np : int
          Scheme order and number of solution nodes on the reference element

      - nvar : int
          Number of solution variables.

      - uh, uh1, uh2, unew : array (nx, ny, nvar, kp1, kp1)
          Solution vectors

      - k1, k2, k3, k4 : array(nx, ny, nvar, kp1, kp1)
          RHS storage vectors

      - fd : array(nx, ny, nvar, kp1, kp1)
          Discontinuous flux at the solution nodes

      - fi : array(nfaces, kp1, nvar)
          Interface/Interaction flux

      - xr, yr : array(kp1, 1)
          Solution nodes on the reference element

      - xc : array(nx, ny)
          Physical coordinates for the cell centers

      - Jn : array(nx, ny, kp1, kp1)
          Element transformation Jacobian

      - dr : array(kp1, kp1)
          Lagrange polynomial differentiation matrix

    """
    def __init__(self, xl, xr, yl, yr, nx, ny, order, nvar=1):

        # grid spacing
        self.dx = dx = float(xr-xl)/nx
        self.dy = dy = float(yr-yl)/ny
        
        # add two ghost elements
        self.nx = nx = nx + 2
        self.ny = ny = ny + 2
        
        # create the grid center coordinates
        xc = np.arange(xl-dx/2, xr+dx, dx)
        yc = np.arange(yl-dy/2, yr+dy, dy)
        
        self.xc, self.yc = np.meshgrid(xc, yc)
        self.x = self.xc

        # solution order
        self.k = k = order
        self.kp1 = kp1 = order + 1
        self.Np = 0.5*(k+1)*(k+2)

        # number of variables
        self.nvar = nvar

        # solution at the nodes and rhs storage vectors
        self.uh = np.zeros(shape=(nx, ny, nvar, kp1, kp1))
        self.u0 = self.uh.copy()
        self.uh1= self.uh.copy()
        self.uh2= self.uh.copy()
        self.k1 = self.uh.copy()
        self.k2 = self.uh.copy()

        # cell averages and cell centers
        self.uavg = np.zeros( shape=(nx, ny, nvar) )

        # new solution vector
        self.unew = self.uh.copy()

        # auxillary solution variable and common solution value
        self.nfaces = nfaces = 4
        self.qx = np.zeros(shape=(nx, ny, nvar, kp1, kp1))
        self.qy = np.zeros(shape=(nx, ny, nvar, kp1, kp1))
        self.uc = np.zeros(shape=(nx, ny, nvar, nfaces, kp1))

        # modal coefficients
        self.uhat = self.uh.copy()

        # physical coordinates of the nodes
        self.xh = np.zeros(shape=(nx, ny, kp1, kp1))
        self.yh = np.zeros(shape=(nx, ny, kp1, kp1))

        # discontinuous flux at the nodes
        self.fd = np.zeros(shape=(nx, ny, nvar, kp1, kp1))
        self.gd = np.zeros(shape=(nx, ny, nvar, kp1, kp1))

        # interface flux values
        self.fi = np.zeros(shape=(nx, ny, nvar, nfaces, kp1))
        self.gi = np.zeros(shape=(nx, ny, nvar, nfaces, kp1))

        ####### Grid related arrays #######
        # # face to element mapping
        self.f2e = np.zeros(shape=(nx, ny, self.nfaces, 2), dtype=int)

        # # element to face mapping
        self.e2f = np.zeros(shape=(nx, ny, 4), dtype=int)

        self.xpsi = np.zeros( shape=(nx, ny, kp1, kp1) )
        self.xeta = np.zeros( shape=(nx, ny, kp1, kp1) )

        self.ypsi = np.zeros( shape=(nx, ny, kp1, kp1) )
        self.yeta = np.zeros( shape=(nx, ny, kp1, kp1) )
        
        self.Jn = np.zeros( shape=(nx, ny, kp1, kp1) )

        # initialize the grid
        self._initialize()

        # Vandermode and Mass matrix
        self.V = V = poly.get_vandermode1d(self.xr, self.k)
        self.Vinv = np.linalg.inv(V)
        self.VinvT = self.Vinv.T

        self.Minv = V.dot(V.T)

    def _initialize(self):
        self._get_node_distribution()
        #self._construct_element_mappings()
        self._construct_grid()

    def _get_node_distribution(self):
        """Get the node distribution on the reference element"""
        # k+1 nodes on the reference element
        self.xr = poly.get_gauss_lobatto_nodes(self.k)
        self.yr = poly.get_gauss_lobatto_nodes(self.k)

        # Lagrange Ploynomial derivative matrix
        self.dr = poly.get_lagrange_derivative_matrix(self.xr, self.k)
        self.drt = self.dr.T

        # get the Lobatto quadrature weights
        self.wts = poly.get_lobatto_quadrature_weights(self.xr)

    # def _construct_element_mappings(self):
    #     """Construct the face to element and element to face mappings"""
    #     nfaces = self.nfaces
    #     ncells = self.ncells
    #     f2e = self.f2e
    #     e2f = self.e2f

    #     # construct the face to element mappings. The ghost nodes are
    #     # used to impose the boundary conditions on the left and right
    #     # ends. A negative index means an invalid index
    #     for i in range(nfaces):
    #         if i == 0:
    #             f2e[i, 0] = -1
    #             f2e[i, 1] = i
    #         elif i == nfaces-1:
    #             f2e[i, 0] = i-1
    #             f2e[i, 1] = -1
    #         else:
    #             f2e[i, 0] = i-1
    #             f2e[i, 1] = i

    #     # construct the element to face mappings
    #     for i in range(ncells):
    #         e2f[i, 0] = i
    #         e2f[i, 1] = i+1
            
    def _construct_grid(self):
        """Construct the grid"""
        nx, ny = self.nx, self.ny

        # Physical coordinates for the nodes of the elements
        xh = self.xh; yh = self.yh

        # spacings along either direction
        dx = self.dx; dy = self.dy

        # nodal points within the element
        nodesx = self.xr; nodesy = self.yr

        dxmin = dymin = 1e10

        for i in range(nx):
            for j in range(ny):
                xc = self.xc[i, j]; yc = self.yc[i, j]
                
                xl = xc - dx/2; xr = xc + dx/2
                yl = yc - dy/2; yr = yc + dy/2

                _x = 0.5*(1-nodesx)*xl + 0.5*(1+nodesx)*xr
                _y = 0.5*(1-nodesy)*yl + 0.5*(1+nodesy)*yr

                dxmin = min(dxmin, np.min( _x[1:] - _x[:-1] ))
                dxmin = min(dymin, np.min( _y[1:] - _y[:-1] ))
                
                xh[i, j], yh[i, j] = np.meshgrid(_x, _y)
                
                # Jacobian and other grid metrics
                self.xpsi[i,j] = self.dr.dot( xh[i, j].T )
                self.xeta[i,j] = self.dr.dot( xh[i, j] )

                self.ypsi[i,j] = self.dr.dot( yh[i, j].T )
                self.yeta[i,j] = self.dr.dot( yh[i, j] )

                self.Jn[i, j] = self.xpsi[i,j]*self.yeta[i,j] -\
                                self.xeta[i,j]*self.ypsi[i,j]

        self.dxmin = dxmin
        self.dymin = dymin

    ###################### Public Interface ######################
    def gradient(self, phi, phix, phiy, var=0):
        """Compute the gradient of field"""
        
        # get the metric quantities
        xpsi = self.xpsi; xeta = self.xeta
        ypsi = self.ypsi; yeta = self.yeta
        Jn = self.Jn

        for i in range(self.nx):
            for j in range(self.ny):
                
                phi_ij = phi[i, j, var]

                phi_psi = phi_ij.dot(self.drt)
                phi_eta = self.dr.dot(phi_ij)

                phix[i, j, var] = 1./Jn[i, j]*( phi_psi*yeta[i,j] - phi_eta*ypsi[i,j])
                phiy[i, j, var] = 1./Jn[i, j]*(-phi_psi*xeta[i,j] + phi_eta*xpsi[i,j])

    def compute_cell_averages(self, u):
        for i in range(self.nx):
            for j in range(self.ny):
                
                # peform integration for each variable
                for var in range(self.nvar):

                    average = 0.0
                    # compute thee running sum using nodal values and
                    # weights
                    for l in range(self.kp1):
                        for m in range(self.kp1):
                            
                            average += self.Jn[i, j, l, m]*self.wts[l]*self.wts[m]*u[i, j, var][l, m]
                                
                    # normalize with respect to the cell volume
                    self.uavg[i, j] = average/(self.dx*self.dy)

    # def get_modal_coefficients(self, u):
    #     for i in range(self.ncells):
    #         for var in range(self.nvar):
    #             self.uhat[i, var, :] = self.Vinv.dot( u[i, var, :] )

    # def get_nodal_coefficients(self, uhat, u):
    #     for i in range(self.ncells):
    #         for var in range(self.nvar):
    #             u[i, var, :] = self.V.dot( uhat[i, var, :] )
