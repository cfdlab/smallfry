class AdvectionDiffusion2DFlux(object):
    def __init__(self, grid, kappa=1.0, beta=0.5):
        self.grid = grid
        self.kappa = kappa
        self.beta = beta

    def get_discontinuous_flux(self, u):
        grid = self.grid
        nvar = grid.nvar

        xpsi = grid.xpsi; xeta = grid.xeta
        ypsi = grid.ypsi; yeta = grid.yeta

        qx = grid.qx; qy = grid.qy

        for var in range(nvar):
            
            for i in range(1, grid.nx-1):
                for j in range(1, grid.ny-1):

                    grid.fd[i, j, var][:] =  yeta[i, j]*(u[i, j, var] - qx[i, j, var]) + \
                        - xeta[i, j]*(u[i, j, var] - qy[i, j, var])

                    grid.gd[i, j, var][:] = -ypsi[i, j]*(u[i, j, var] - qx[i, j, var]) +\
                        + xpsi[i, j]*(u[i, j, var] - qy[i, j, var])
                    
    def get_interaction_flux(self, u):
        grid = self.grid; kappa = self.kappa

        xpsi = grid.xpsi; xeta = grid.xeta
        ypsi = grid.ypsi; yeta = grid.yeta

        beta = self.beta

        qx = grid.qx; qy = grid.qy        
        
        for i in range(1, grid.nx-1):
            for j in range(1, grid.ny-1):

                # get the solution variables at the four neighboring cells
                up = u[i, j]
                uw = u[i,j-1] # west
                ue = u[i,j+1] # east
                ut = u[i+1,j] # top
                ub = u[i-1,j] # bottom

                # get the auxillary variable at the four neighboring
                # cells
                qxp = qx[i, j]; qyp = qy[i, j]
                qxw = qx[i, j-1]; qyw = qy[i, j-1]
                qxe = qx[i, j+1]; qye = qy[i, j+1]
                qxt = qx[i+1, j]; qyt = qy[i+1, j]
                qxb = qx[i-1, j]; qyb = qy[i-1, j]
                
                # go over each face and compute the interaction
                # flux. We assume an outward pointing normal
                for var in range(grid.nvar):

                    # Bottom
                    urgt = ub[var][-1, :]
                    ulft = up[var][ 0, :]
                    
                    qxlft = qxp[var][ 0, :]; qylft = qyp[var][ 0, :]
                    qxrgt = qxb[var][-1, :]; qyrgt = qyb[var][-1, :]

                    f = 0.5*(ulft + urgt) + 0.5*kappa*(urgt-ulft)
                    g = 0.5*(ulft + urgt) + 0.5*kappa*(urgt-ulft)
                    
                    fdiff = -0.5*( qxlft + qxrgt ) + beta*(-qxrgt - (-qxlft))
                    gdiff = -0.5*( qylft + qyrgt ) + beta*(-qyrgt - (-qylft))

                    grid.fi[i, j, var, 0][:] =  yeta[i,j, 0, :]*(f+fdiff) - xeta[i,j, 0, :]*(g+gdiff)
                    grid.gi[i, j, var, 0][:] = -ypsi[i,j, 0, :]*(f+fdiff) + xpsi[i,j, 0, :]*(g+gdiff)

                    # East
                    urgt = ue[var][:,  0]
                    ulft = up[var][:, -1]
                    
                    qxlft = qxp[var][:, -1]; qylft = qyp[var][:, -1]
                    qxrgt = qxe[var][:,  0]; qyrgt = qye[var][:,  0]
                    
                    f = 0.5*(ulft + urgt) + 0.5*kappa*(ulft-urgt)
                    g = 0.5*(ulft + urgt) + 0.5*kappa*(ulft-urgt)

                    fdiff = -0.5 * (qxlft + qxrgt) + beta*(-qxlft - (-qxrgt))
                    gdiff = -0.5 * (qylft + qyrgt) + beta*(-qylft - (-qyrgt))

                    grid.fi[i, j, var, 1][:] =  yeta[i,j, :, -1]*(f+fdiff) - xeta[i, j, :, -1]*(g+gdiff)
                    grid.gi[i, j, var, 1][:] = -ypsi[i,j, :, -1]*(f+fdiff) + xpsi[i, j, :, -1]*(g+gdiff)

                    # Top
                    urgt = ut[var][ 0, :]
                    ulft = up[var][-1, :]

                    qxlft = qxp[var][-1, :]; qylft = qyp[var][-1, :]
                    qxrgt = qxt[var][0 , :]; qyrgt = qyt[var][0 , :]

                    f = 0.5*(ulft + urgt) + 0.5*kappa*(ulft-urgt)
                    g = 0.5*(ulft + urgt) + 0.5*kappa*(ulft-urgt)

                    fdiff = -0.5*(qxlft + qxrgt) + beta*(-qxlft - (-qxrgt))
                    gdiff = -0.5*(qylft + qyrgt) + beta*(-qylft - (-qyrgt))

                    grid.fi[i, j, var, 2][:] =  yeta[i, j, -1, :]*(f+fdiff) - xeta[i, j, -1, :]*(g+gdiff)
                    grid.gi[i, j, var, 2][:] = -ypsi[i, j, -1, :]*(f+fdiff) + xpsi[i, j, -1, :]*(g+gdiff)

                    # West
                    urgt = uw[var][:, -1]
                    ulft = up[var][:,  0]

                    qxlft = qxp[var][:,  0]; qylft = qyp[:,  0]
                    qxrgt = qxw[var][:, -1]; qyrgt = qyw[:, -1]

                    f = 0.5*(ulft + urgt) + 0.5*kappa*(urgt-ulft)
                    g = 0.5*(ulft + urgt) + 0.5*kappa*(urgt-ulft)

                    fdiff = -0.5 * (qxlft + qxrgt) + beta*(-qxrgt - (-qxlft))
                    gdiff = -0.5 * (qylft + qyrgt) + beta*(-qyrgt - (-qylft))

                    # print "Interaction flux :: ", i, j
                    # print j-1, qxrgt, j, qxlft
                    # print fdiff
                    # print gdiff
                    # print qxw[var]
                    # print "######"

                    grid.fi[i, j, var, 3][:] =  yeta[i, j, :, 0]*(f+fdiff) - xeta[i, j, :, 0]*(g+gdiff)
                    grid.gi[i, j, var, 3][:] = -ypsi[i, j, :, 0]*(f+fdiff) + xpsi[i, j, :, 0]*(g+gdiff)
