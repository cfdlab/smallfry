"""General Solver class for FR type schemes

The solver's role is to marshall the simulation by repeatedly calling
all ancillary functions and the main integration routine to advance
the solution to the next time step.

The ancillary functions handle and I/O and call any pre and post
integration routines.

The integration of the state variables is handled jointly by the
Integrator and the function (class) responsible for computing the
accelerations (RHS).

"""
from optparse import OptionParser, OptionGroup, Option
from os.path import basename, splitext, abspath, join
from time import time
import sys

import numpy
from utils import mkdir, update_progress

# Different integration
class IntegrationScheme:
    Euler = 0
    RK4 = 1
    TVDRK3 = 2    

class Solver(object):
    """Main solver object object that marshalls the simulation"""
    def __init__(
        self, dim, grid, tf=1.0, cfl=0.1, integration=IntegrationScheme.TVDRK3,
        pfreq=100, iteration_count=0, current_time=0.,
        constant_dt=True, fname=None):
        """Constructor

        Parameters:

        dim : int
            Problem dimensionality

        grid : grid1d.Grid1D
            numerical grid

        tf : double
            Final time

        cfl : double (0.5)
            CFL number for time-step control

        integration : (int) IntegrationScheme
            Type of integration scheme to advance the solution
            
        pfreq : int
            Output print frequency

        iteration_count : int (0)
            Initial value for the  counter. This is needed for restarts

        current_time : double (0.)
            Initial value for the time counter. This is needed for restarts

        constant_dt : bool (True)
            Flag to use constant time steps

        """
        # problem dimensionality and grid
        self.dim = dim
        self.grid = grid

        # timing attributes and I/O defaults
        self.tf=tf
        self.cfl = cfl
        self.constant_dt = constant_dt

        # solver iteration count and output print frequency
        self.iteration_count=iteration_count
        self.current_time=current_time
        self.pfreq = pfreq
        
        # output file name
        if fname is None:
            fname = sys.argv[0].split('.')[0]
        self.fname=fname

        # the remaining are the arguments
        self.args = sys.argv[1:]

        # setup the option parser
        self._setup_optparse()

        # list of pre and post intergation functions
        self.pre_integration_functions = []
        self.post_integration_functions = []

        # integration scheme
        self.integration = integration
        self._setup_integrator()

    def set_rhs_eval(self, rhs_eval):
        """Set the object to compute the RHS (accelerations)
        
        The RHS is determined by the algorithm, that is, either FR, DG
        or any other type of algorithm. These are implemented in
        `rhs_eval.py`

        The object should define a method `evaluate` that accepts the
        current solution vector, the storage vector for the
        accelerations and an optional stage argument indicating the
        integration stage.
        
        """
        self.rhs_eval = rhs_eval

    def solve(self):
        """Marshalling routine for the simulation

        In every iteration step, the solver does the following:
            - call output if time is right
            - call get_time_step to get the new stable time step
            - call pre integration routines
            - call integrate to advance the solution
            - call post integration routines

        """
        tf = self.tf
        current_time = self.current_time
        iteration_count = self.iteration_count

        # the grid, integrator and rhs_eval
        grid = self.grid
        rhs_eval = self.rhs_eval
        #integrator = self.integrator

        # get the initial stable time step
        dt = rhs_eval.get_time_step(self.cfl)

        time_counter = 0.0
        while current_time < tf:
            # I/O
            if ( (iteration_count % self.pfreq) == 0 ):
                self.output(iteration_count, current_time, dt)

            # calculate new time step and adjust if necessary
            if not self.constant_dt:
                dt = rhs_eval.get_time_step(self.cfl)

            if ( current_time + dt > tf ):
                print '\nAdjusting dt to land on final time...'
                dt = tf - current_time

            # call any functions prior to the integration stage
            for func in self.pre_integration_functions:
                func.evaluate( iteration_count, current_time, dt )
                
            ############ Begin Integration ############
            # copy current level solution
            grid.u0[:] = grid.uh[:]

            # Evaluate Stage 1 and update
            rhs_eval.evaluate(grid.uh, grid.k1, stage=1)
            grid.uh1[:] = grid.u0[:] + dt * grid.k1[:]
            
            # # # Evaluate Stage 2 and update
            rhs_eval.evaluate(grid.uh1, grid.k2, stage=2)
            grid.uh2[:] = 0.75*grid.u0[:] + 0.25 * (grid.uh1[:] + dt*grid.k2[:])

            # Evaluate Stage 3 and final update
            rhs_eval.evaluate(grid.uh2, grid.k1, stage=3)
            grid.unew[:] = 1./3.*grid.u0[:] + 2./3. * (grid.uh2[:] + dt*grid.k1[:])
            ############ End Integration ############

            # swap solution buffers
            grid.uh[:] = grid.unew[:]

            # update counters
            current_time += dt
            iteration_count += 1

            # call any post-integrator functions
            for func in self.post_integration_functions:
                func.evaluate( iteration_count, current_time, dt )

        # final output
        self.output(iteration_count, current_time, dt)
        self.current_time = current_time
                
    def set_pre_integration_function(self, func):
        self.pre_integration_functions.append(func)

    def set_post_integration_function(self, func):
        self.post_integration_functions.append(func)

    def output(self, iteration_count, current_time, dt):
        fnamebase = self.fname + '_%03d'%iteration_count
        fname = join( self.path, fnamebase )

        # use NumPy's savez to save the output
        print 'Writing output %s at time %g s'%(fname, current_time)

        grid = self.grid
        grid.compute_cell_averages(grid.uh)

        if self.dim == 2:
            numpy.savez(fname, dt=dt,
                        iteration_count=iteration_count, current_time=current_time,
                        x=grid.x, xh=grid.xh, yh=grid.yh, uh=grid.uh, f2e=grid.f2e, e2f=grid.e2f,
                        uavg=grid.uavg, xc=grid.xc, yc=grid.yc, V=grid.V, 
                        Vinv=grid.Vinv, Minv=grid.Minv, xr=grid.xr, u0=grid.u0,
                        qx=grid.qx, qy=grid.qy, uc=grid.uc, k1=grid.k1, Jn=grid.Jn, dr=grid.dr,
                        fi=grid.fi, gi=grid.fi)
        else:
            numpy.savez(fname, dt=dt,
                    iteration_count=iteration_count, current_time=current_time,
                    x=grid.x, xh=grid.xh, uh=grid.uh, f2e=grid.f2e, e2f=grid.e2f,
                    uavg=grid.uavg, xc=grid.xc, V=grid.V, 
                    Vinv=grid.Vinv, Minv=grid.Minv, xr=grid.xr, u0=grid.u0,
                    q=grid.q, uc=grid.uc, k1=grid.k1, Jn=grid.Jn, dr=grid.dr)

    def setup(self):
        """Process all command line options"""
        self._process_command_line()
        options = self.options

        # set the options
        self.fname = options.fname
        self.pfreq = options.pfreq

        # set the acceleration function cfl
        if options.cfl is not None:
            self.cfl = cfl

        # use constant time steps
        self.constant_dt = options.constant_dt
        
        # final time
        if options.tf:
            self.tf = options.tf

    def integrate(self, current_time, iteration_count, dt):
        raise RuntimeError('Solver::integrate called')

    def _setup_integrator(self):
        integration = self.integration
        grid = self.grid

    def _process_command_line(self):
        (options, args) = self.parser.parse_args(self.args)
        self.options = options
        
        #save the path where we want to dump output
        self.path = abspath(options.outdir)
        mkdir(self.path)        
                    
    def _setup_optparse(self):
        usage = """
        python %prog [options] """
        self.parser = parser = OptionParser(usage)

        # Add some default options
        parser.add_option("--directory", action="store", type="string",
                         dest="outdir", default=self.fname+'_output',
                         help="""The output directory""")

        parser.add_option("--fname", action="store", type="string",
                         dest="fname", default=self.fname,
                         help="Output file name")

        parser.add_option("--pfreq", action="store", type="int",
                         dest="pfreq", default=self.pfreq,
                         help="Output print frequency")

        time = OptionGroup(parser, "Time Options", "Time step options")

        time.add_option("--tf", action="store", type="float",
                            dest="tf", default=None,
                            help="Final time")

        time.add_option("--cfl", action='store', type='float',
                        dest='cfl', default=None, help='CFL number')

        time.add_option("--constant-dt", action="store_true", 
                        dest="constant_dt", default=self.constant_dt,
                        help="Use a constant time step")

        parser.add_option_group(time)
