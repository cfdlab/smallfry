from smallfry.grid import polynomials as poly
import numpy as np

class RHSEval2D(object):
    def __init__(self, grid, flux):
        self.grid = grid
        self.flux = flux

        # perform additional initializations
        self.initialize()

        # limiter function
        self.limiter_func = None

    def set_limiter(self, func):
        self.limiter_func = func

class FluxReconstructionEval2D(RHSEval2D):
    def __init__(self, grid, flux, diffusion=False):
        
        self.diffusion = diffusion
        self.beta = flux.beta
        super(FluxReconstructionEval2D,self).__init__(grid, flux)

    def initialize(self):
        # get the grid and the solution nodes on the reference element
        grid = self.grid
        xr = grid.xr

        # get the polynomial correction fucntions
        gl = poly.radau_left(grid.k)
        gr = poly.radau_right(grid.k)

        # derivatives of the correction functions
        glderiv = gl.deriv(); self.dgl = glderiv(grid.xr)
        grderiv = gr.deriv(); self.dgr = grderiv(grid.xr)

        # correction fluxes along the faces
        self.fc_b = np.zeros(shape=(grid.kp1, grid.kp1))
        self.fc_e = np.zeros(shape=(grid.kp1, grid.kp1))
        self.fc_t = np.zeros(shape=(grid.kp1, grid.kp1))
        self.fc_w = np.zeros(shape=(grid.kp1, grid.kp1))

    def get_time_step(self, cfl):
        return 0.5*cfl * self.grid.dxmin

    def evaluate(self, u, k, stage=1):
        grid = self.grid
        qx = grid.qx; qy = grid.qy
        
        # limit the solution
        if self.limiter_func is not None:
            self.limiter_func.limit(u)

        # impose boundary (periodic) conditions
        for var in range(grid.nvar):
            for i in range(grid.nx):
                u[i, 0, var][:] = u[i, -2, var]
                u[i, -1,var][:] = u[i, 1 , var]

            for j in range(grid.ny):
                u[0, j, var][:] = u[-2, j, var]
                u[-1,j, var][:] = u[1 , j, var]
            
        #u[0, :][:] = u[-2, :]
        #u[-1,:][:] = u[1,  :]

        if self.diffusion:
            # compute the common solution value
            self.compute_common_solution_value(u)

            # get the auxillary variable
            self.get_auxillary_variable(u)

            # impose boundary (periodic) conditions on the auxillary
            # variable
            for var in range(grid.nvar):
                for i in range(grid.nx):
                    qx[i, 0, var][:] = qx[i, -2, var]
                    qx[i, -1,var][:] = qx[i, 1 , var]

                    qy[i, 0, var][:] = qy[i, -2, var]
                    qy[i, -1,var][:] = qy[i, 1 , var]

                for j in range(grid.ny):
                    qx[0, j, var][:] = qx[-2, j, var]
                    qx[-1,j, var][:] = qx[1 , j, var]

                    qy[0, j, var][:] = qy[-2, j, var]
                    qy[-1,j, var][:] = qy[1 , j, var]

        # get the transformed discontinuous flux
        self.flux.get_discontinuous_flux(u)
        
        # get the interaction flux
        self.flux.get_interaction_flux(u)
            
        # evaluate the RHS
        self._evaluate(u, k)

    def compute_common_solution_value(self, u):
        grid = self.grid
        beta = self.beta
        uc = grid.uc
        
        for i in range(1,grid.nx-1):
            for j in range(1, grid.ny-1):

                # get the solution variables at the four neighboring cells
                up = u[i, j]
                uw = u[i,j-1] # west
                ue = u[i,j+1] # east
                ut = u[i+1,j] # top
                ub = u[i-1,j] # bottom
                
                for var in range(grid.nvar):
                    
                    # Bottom
                    ulft = up[var][0, :]
                    urgt = ub[var][-1,:]
                    
                    uc[i, j, var, 0, :] = 0.5*(ulft + urgt) - beta*(urgt-ulft)

                    # East
                    ulft = up[var][:,-1]
                    urgt = ue[var][:, 0]
                    
                    uc[i, j, var, 1, :] = 0.5*(ulft + urgt) - beta*(ulft-urgt)

                    # Top
                    ulft = up[var][-1,:]
                    urgt = ut[var][0, :]
                    
                    uc[i, j, var, 2, :] = 0.5*(ulft + urgt) - beta*(ulft-urgt)

                    # West
                    ulft = up[var][:, 0]
                    urgt = uw[var][:,-1]

                    uc[i, j, var, 3, :] = 0.5*(ulft + urgt) - beta*(urgt-ulft)

    def get_auxillary_variable(self,u):
        grid = self.grid
        uc = grid.uc; qx = grid.qx; qy = grid.qy

        dgl = self.dgl; dgr = self.dgr

        xpsi = grid.xpsi; xeta = grid.xeta
        ypsi = grid.ypsi; yeta = grid.yeta

        for i in range(grid.nx):
            for j in range(grid.ny):
                
                for var in range(grid.nvar):

                    # gradients in the local coordinate system
                    q_psi = u[i, j, var].dot(grid.drt)
                    q_eta = grid.dr.dot(u[i, j, var] )

                    # correction terms
                    for l in range(grid.kp1):

                        # bottom 
                        q_eta[:, l] += (uc[i, j, var, 0, l] - u[i, j, var, 0, l])*self.dgl

                        # top
                        q_eta[:, l] += (uc[i, j, var, 2, l] - u[i, j, var, -1,l])*self.dgr

                        # west
                        q_psi[l, :] += (uc[i, j, var, 3, l] - u[i, j, var, l, 0])*self.dgl

                        # east
                        q_psi[l, :] += (uc[i, j, var, 1, l] - u[i, j, var, l,-1])*self.dgr

                    # gradients in the global (x,y) coordinate system
                    qx[i, j, var][:] = 1./grid.Jn[i, j]*( q_psi*yeta[i,j] + q_eta*xeta[i,j])
                    qy[i, j, var][:] = 1./grid.Jn[i, j]*(-q_psi*ypsi[i,j] + q_eta*xpsi[i,j])

    def _evaluate(self, u, k):
        grid = self.grid

        # number of cells and variables
        nx = grid.nx; ny = grid.ny; nvar = grid.nvar

        # flux values at the nodes and interfaces
        fd = grid.fd; gd = grid.gd
        fi = grid.fi; gi = grid.gi

        # derivatives of the correction functions
        dgl = self.dgl; dgr = self.dgr

        # flux correction matrices
        fc_b = self.fc_b
        fc_e = self.fc_e
        fc_t = self.fc_t
        fc_w = self.fc_w        
        
        # compute rhs on a per-element basis
        for i in range(grid.nx):
            for j in range(grid.ny):
                
                for var in range(nvar):
                    # discontinuous contribution
                    k[i, j, var][:] = -(fd[i, j, var].dot(grid.drt) + grid.dr.dot(gd[i, j, var]))
                
                    # correction terms
                    for l in range(grid.kp1):
                        # west
                        fc_w[l, :] = -(fi[i, j, var, 3, l] - fd[i, j, var, l, 0 ])*dgl

                        # east
                        fc_e[l, :] = -(fi[i, j, var, 1, l] - fd[i, j, var, l, -1])*dgr

                        # bottom
                        fc_b[:, l] = -(gi[i, j, var, 0, l] - gd[i, j, var, 0, l ])*dgl

                        # top
                        fc_t[:, l] = -(gi[i, j, var, 2, l] - gd[i, j, var, -1, l])*dgr

                    # add the correction terms
                    k[i, j, var][:] += fc_e + fc_w + fc_b + fc_t

                    # divide by the Jacobian
                    k[i, j, var][:] /= grid.Jn[i, j]
