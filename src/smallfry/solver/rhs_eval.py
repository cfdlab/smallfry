import smallfry.grid.polynomials as poly
import numpy as np

def minmod(a):
    """Minmod function"""
    
    s = np.sum(np.sign(a))/len(a)
    abss = np.abs(s)
    if abss == 1:
        return s * np.min( np.abs(a) )
    else:
        return 0

def maxmod(a):
    """Maxmod function"""
    
    s = np.sum( np.sign(a) )/len(a)
    abss = np.abs(s)
    if abss == 1:
        return s * np.max( np.abs(a) )
    else:
        return 0

class FRCorrectionFunction:
    Radau = 0

class CockburnShuLimiterMUSCL(object):
    def __init__(self, grid):
        self.grid = grid

    def limit(self, u):
        grid = self.grid
        
        # compute the cell averages which will be used for limiting
        grid.compute_cell_averages(u)

        uavg = grid.uavg
        
        # iterate over each cell and limit the solution
        for i in range(1, grid.ncells-1):
            for var in range(grid.nvar):
                
                # get the averaged variables on the 3 cells
                ui = uavg[i, var]; um1 = uavg[i-1, var]; up1 = uavg[i+1, var]
                
                # get the values at the ends of this cell
                uleft = u[i, var,  0]; urght = u[i, var, -1]

                # reconstruct left and right edge values
                v1 = ui - minmod( np.array([ui-uleft, ui-um1, up1-ui]) )
                v2 = ui + minmod( np.array([urght-ui, ui-um1, up1-ui]) )
                
                # ascertain if the variable needs limiting
                if ( (np.abs(v1-uleft) > 1e-8) or (np.abs(v2-urght) > 1e-8) ):

                    # create a piecewise linear solution 
                    uh = grid.Vinv.dot( u[i, var, :] )
                    uh[2:] = 0.0
                    ul = grid.V.dot(uh)

                    # first derivative of the linear solution
                    ux = grid.dr.dot( ul )/grid.Jn[i]

                    x = grid.xh[i]; h = grid.dx; hi = 1./h
                    x0 = grid.xc[i]

                    # limit the solution within the element
                    arr = np.concatenate( (ux, np.array([hi*(up1-ui), hi*(ui-um1)])) )
                    slope = minmod( arr )

                    # limited value
                    u[i, var][:] = ui + (x-x0)*slope

class BSBLimiter(object):
    def __init__(self, grid):
        self.grid = grid

    def limit(self, u):
        grid = self.grid
        m = grid.kp1
        
        # get the modal coefficients
        grid.get_modal_coefficients(u)
        
        # iterate over each cell and limit the solution
        for i in range(1, grid.ncells-1):
            for var in range(grid.nvar):
                
                # get the modal coefficients that will be limited
                uhi = grid.uhat[i, var, :]
                uhip1 = grid.uhat[i+1, var, :]
                uhim1 = grid.uhat[i-1, var, :]
                
                # iteratively limit each modal component of the solution
                for l in range(grid.k-1, -1, -1):                
                    arr = np.array([ (2*l+1)*uhi[l+1], uhip1[l]-uhi[l], uhi[l]-uhim1[l] ])
                    tmp = 1./(2*m+1) * minmod( arr )
                    
                    if tmp != uhi[l+1]:
                        wp = uhip1[l] - (2*l+1)*uhip1[l+1]
                        wm = uhim1[l] + (2*l+1)*uhim1[l+1]

                        arr = np.array([ (2*l+1)*uhi[l+1], wp-uhi[l], uhi[l]-wm ])
                        tmp1 = 1./(2*m+1) * minmod(arr)
                
                        uhi[l+1] = maxmod( np.array([tmp, tmp1]) )

        # now go back to the nodal coefficients
        grid.get_nodal_coefficients(grid.uhat, u)

class RHSEval1D(object):
    def __init__(self, grid, flux):
        self.grid = grid
        self.flux = flux

        # perform additional initializations
        self.initialize()

        # limiter function
        self.limiter_func = None

    def set_limiter(self, func):
        self.limiter_func = func

    def evaluate(self, u, k, stage=1):
        grid = self.grid
        
        # limit the solution
        if self.limiter_func is not None:
            self.limiter_func.limit(u)

        # impose boundary conditions
        u[0][:] = u[-2]
        u[-1][:] = u[1]
        
        # get the transformed discontinuous flux
        self.flux.get_discontinuous_flux(u)
        
        # get the interaction flux
        self.flux.get_interaction_flux(u)
            
        # evaluate the RHS
        self._evaluate(u, k)

    def get_time_step(self, cfl):
        return self.flux.get_time_step(cfl)

    def initialize(self):
        pass

class FVMEval1D(RHSEval1D):
    def _evaluate(self, u, k):
        grid = self.grid

        ncells = grid.ncells; nvar = grid.nvar

        # flux at the nodes and interfaces
        fd = grid.fd; fi = grid.fi

        # compute rhs on a per-element basis
        for i in range(1, ncells-1):
            fl, fr = grid.e2f[i]

            for var in range(nvar):
                # comute the rhs
                k[i, var][:] = ( fi[fl, var] - fi[fr, var] )/grid.dx

class NodalDGEval1D(RHSEval1D):
    def _evaluate(self, u, k):
        grid = self.grid

        # number of cells and variables
        ncells = grid.ncells; nvar = grid.nvar

        # flux values at the nodes and interfaces
        fd = grid.fd; fi = grid.fi

        # Inverse of Mass matrix
        Minv = grid.Minv

        # compute rhs on a per-element basis
        for i in range(1, ncells-1):
            fl, fr = grid.e2f[i]

            for var in range(nvar):
                
                # comute the rhs
                k[i, var][:] = ( -grid.dr.dot(fd[i, var]) + \
                                 ( fd[i,var,-1] - fi[fr,var] ) * Minv[:,-1] -\
                                 ( fd[i,var, 0] - fi[fl,var] ) * Minv[:, 0] )/grid.Jn[i]

class FluxReconstructionEval1D(RHSEval1D):
    def __init__(
        self, grid, flux, correction_func=FRCorrectionFunction.Radau, 
        diffusion=False):
        
        self.diffusion = diffusion
        self.correction_func = correction_func
        super(FluxReconstructionEval1D,self).__init__(grid, flux)

    def initialize(self):
        # get the grid and the solution nodes on the reference element
        grid = self.grid
        xr = grid.xr

        # get the polynomial correction fucntions
        gl = poly.radau_left(grid.k)
        gr = poly.radau_right(grid.k)

        # derivatives of the correction functions
        glderiv = gl.deriv(); self.dgl = glderiv(grid.xr)
        grderiv = gr.deriv(); self.dgr = grderiv(grid.xr)

        self.gl = gl(grid.xr)
        self.gr = gr(grid.xr)

    def evaluate(self, u, k, stage=1):
        grid = self.grid
        
        # limit the solution
        if self.limiter_func is not None:
            self.limiter_func.limit(u)

        # impose boundary conditions
        u[0][:] = u[-2]
        u[-1][:] = u[1]

        if self.diffusion:
            # compute the common solution value
            self.compute_common_solution_value(u)

            # get the auxillary variable
            self.get_auxillary_variable(u)

            # impose boundary conditions on the auxillary variable
            grid.q[0][:] = grid.q[-2]
            grid.q[-1][:] = grid.q[1]
        
        # get the transformed discontinuous flux
        self.flux.get_discontinuous_flux(u)
        
        # get the interaction flux
        self.flux.get_interaction_flux(u)
            
        # evaluate the RHS
        self._evaluate(u, k)

    def compute_common_solution_value(self, u):
        grid = self.grid

        # number of faces and variables
        nfaces = grid.nfaces; nvar = grid.nvar

        # face to element mapping
        f2e = grid.f2e

        beta = 0.5
        # compute the common solution values at the faces
        for i in range(1, nfaces-1):
            # elements at the left and right of this face
            el, er = f2e[i]

            # compute the common solution value at the face
            for var in range(nvar):
                grid.uc[i, var] = 0.5*( u[el, var, -1] + u[er, var, 0] ) - \
                    beta * ( u[el, var, -1] - u[er, var, 0] )

    def get_auxillary_variable(self, u):
        grid = self.grid

        # number of cells and variables
        ncells = grid.ncells; nvar = grid.nvar

        # common solution value and the auxillary variable
        uc = grid.uc; q = grid.q

        gl = self.gl; gr = self.gr
        dgl = self.dgl; dgr = self.dgr
        
        # compute rhs on a per-element basis
        for i in range(1, ncells-1):
            fl, fr = grid.e2f[i]

            for var in range(nvar):
                # comute the rhs
                q[i, var][:] = (grid.dr.dot(u[i, var]) + \
                                     (uc[fl, var] - u[i,var,  0])*dgl + \
                                     (uc[fr, var] - u[i,var, -1])*dgr)/grid.Jn[i]
    def _evaluate(self, u, k):
        grid = self.grid

        # number of cells and variables
        ncells = grid.ncells; nvar = grid.nvar

        # flux values at the nodes and interfaces
        fd = grid.fd; fc = grid.fc; fi = grid.fi

        gl = self.gl; gr = self.gr
        dgl = self.dgl; dgr = self.dgr
        
        # compute rhs on a per-element basis
        for i in range(1, ncells-1):
            fl, fr = grid.e2f[i]

            for var in range(nvar):
                # store the continuous flux for reference
                fc[i, var][:] = fd[i, var] + \
                                (fi[fl, var] - fd[i,var,  0])*gl + \
                                (fi[fr, var] - fd[i,var, -1])*gr
                
                # comute the rhs
                k[i, var][:] = -(grid.dr.dot(fd[i, var]) + \
                                     (fi[fl, var] - fd[i,var,  0])*dgl + \
                                     (fi[fr, var] - fd[i,var, -1])*dgr)/grid.Jn[i]

class Advection1DFlux(object):
    def __init__(self, grid, a=1.0, kappa=1.0):
        self.grid = grid

        self.a = a
        self.kappa = kappa

    def get_time_step(self, cfl):
        return 0.5*cfl * self.grid.dxmin/self.a

    def get_discontinuous_flux(self, u):
        grid = self.grid
        q = grid.q
        for var in range(grid.nvar):
            grid.fd[:, var, :] = u[:, var, :] - q[:, var, :]

    def get_interaction_flux(self, u):
        kappa = self.kappa
        a = self.a

        grid = self.grid
        q = grid.q

        f2e = grid.f2e
        
        beta = 0.5
        tau = 0.0
        for i in range(1, grid.nfaces-1):
            el, er = f2e[i]
            
            # interface flux
            for var in range(grid.nvar):
                grid.fi[i, var] = 0.5 * (u[el, var, -1] + u[er, var, 0]) + \
                    0.5*kappa * ( u[el, var, -1] - u[er, var, 0] ) +( \
                    0.5 * (-q[el, var, -1] + (-q[er, var, 0])) + \
                    beta* (-q[el, var, -1] - (-q[er, var, 0])) + \
                    tau * ( u[el, var, -1] -   u[er, var, 0]) )
    
class Euler1DFlux(object):
    def __init__(self, grid, gamma=1.4):

        self.grid = grid
        self.gamma = gamma
        self.gamma1 = gamma - 1.0

    def get_time_step(self, cfl):
        grid = self.grid

        gamma = self.gamma
        gamma1 = self.gamma1

        q = grid.uh

        # get the conserved and primitive variables at the nodes
        rho = q[:, 0, :]; M = q[:, 1, :]; E = q[:, 2, :]
        p = gamma1 * (E - 0.5*M*M/rho)
        c = np.sqrt( gamma * p/rho )

        maxvel = np.max(np.abs(M/rho) + c)
        dt = cfl * self.grid.dxmin/maxvel
        return dt

    def get_discontinuous_flux(self, q):
        grid = self.grid

        gamma = self.gamma
        gamma1 = gamma - 1.0

        # get the conserved variables variables
        rho = q[:, 0, :]; M = q[:, 1, :]; E = q[:, 2, :]
        p = gamma1 * (E - 0.5*M*M/rho)

        # set the discontinuous flux from the conserved variables
        grid.fd[:, 0, :] = M[:]
        grid.fd[:, 1, :] = M[:]*M[:]/rho[:] + p[:]
        grid.fd[:, 2, :] = (E + p)*M/rho

    def get_interaction_flux(self, q):
        gamma = self.gamma
        gamma1 = self.gamma - 1.0

        grid = self.grid
        f2e = grid.f2e

        # get the discontinuous & continuous fluxes
        fd = grid.fd
        fi = grid.fi

        # get the conserved and primitive variables at the nodes
        rho = q[:, 0, :]; M = q[:, 1, :]; E = q[:, 2, :]
        p = gamma1 * (E - 0.5*M*M/rho)
        c = np.sqrt( gamma * p/rho )

        maxvel = np.max( np.abs(M/rho) + c )
        
        # iterate over each internal face and set the interaction flux
        for i in range(1, grid.nfaces-1):
            el, er = f2e[i]
            
            # interface fluxes (centered plus diffusive term)
            fi[i, :] = 0.5 * ( fd[el, :, -1] + fd[er, :, 0] ) -\
                       0.5*maxvel * ( q[er,:,0] - q[el,:,-1] )
