#!/usr/bin/env python

from setuptools import find_packages, setup

setup(name="smallfry",
      version="0.1",
      packages = find_packages('src'),
      package_dir = {'': 'src'},
      include_package_data = True,
      )
